import Vue from 'vue'
import Router from 'vue-router'

import About from '@/views/about'
import Portfolio from '@/views/portfolio'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/about'
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/portfolio',
      name: 'Portfolio',
      component: Portfolio
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
  })